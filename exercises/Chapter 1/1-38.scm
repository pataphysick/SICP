#lang sicp

(define (cont-frac-iter n d k)
  (define (iter i result)
    (if (< i 1)
        result
        (iter (- i 1)
              (/ (n i)
                 (+ (d i)
                    result)))))
  (iter k (/ (n k) (d k))))

(define (n i) 1.0)
(define (d i)
  (if (not (= 0 (remainder (+ i 1) 3)))
      1
      (* 2 (/ (+ i 1) 3))))

(define (euler k)
  (+ 2 (cont-frac-iter n d k)))

> (euler 1000)
2.7182818284590455