(define (sos x y)
    (+ (sq x) (sq y)))

(define (sq x) (* x x))

(define (larger-sos a b c)
    (cond ((and (<= a b) (<= a c)) (sos b c))
          ((and (<= b a) (<= b c)) (sos a c))
          ((and (<= c a) (<= c b)) (sos a b))))