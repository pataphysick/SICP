#lang sicp

(define (expmod base exp m)
  (define (wrap x) ;wrapper function to avoid calculating the remainder of the square of expmod twice
    (nontriv x (remainder (square x) m)))
  (define (nontriv x y)
    (if (and (= 1 y)
             (not (= x (- m 1)))
             (not (= x 1)))
        0
        y))
  (cond ((= exp 0) 1)
        ((even? exp)
         (wrap (expmod base (/ exp 2) m)))
        (else
         (remainder
          (* base (expmod base (- exp 1) m))
          m))))

(define (square x) (* x x))

(define (miller-rabin n)
    (define (try-it a)
        (not (= (expmod a (- n 1) n) 0))) ;returns false if expmod returns 0
    (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
    (cond ((= times 0) true)
          ((even? n) false) ;procedure only valid for odd n so explicitly return false if n is even
          ((miller-rabin n) (fast-prime? n (- times 1)))
          (else false)))