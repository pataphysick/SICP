#lang sicp

(define (compose f g)
  (lambda (x) (f (g x))))

(define (repeated f n)
  (define (iter i g)
    (if (>= i n)
        g
        (iter (+ i 1) (compose f g))))
  (iter 1 f))

(define dx .01)

(define (smooth f)
  (lambda (x)
    (/ (+ (f x)
          (f (- x dx))
          (f (+ x dx)))
       3)))
  
(define (n-fold-smooth f n)
  ((repeated smooth n) f))