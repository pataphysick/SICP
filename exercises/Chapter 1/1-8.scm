(define (cbrt-iter guess old-guess x)
  (if (good-enough? guess old-guess x)
      guess
      (cbrt-iter (improve guess x) guess x)))

(define (improve guess x)
    (average guess (/ (+ (/ x 
                            (square guess)) 
                         (* 2 guess)) 
                      3)))

(define (average x y)
    (/ (+ x y) 2))

(define (good-enough? guess old-guess x)
    (< (abs (- (/ guess old-guess) 1)) 0.00001))


(define (cbrt x)
    (cbrt-iter 1.0 2 x))