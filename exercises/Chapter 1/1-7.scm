(define (sqrt-iter guess x)
  (if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x) x)))

(define (improve guess x)
    (average guess (/ x guess)))

(define (average x y)
    (/ (+ x y) 2))

(define (good-enough? guess x)
    (< (abs (- (square guess) x)) 0.001))

(define (sqrt x)
    (sqrt-iter 1.0 x))


;The previous procedure will not work for small numbers because the tolerence we set, 0.001, will become big relative to the numbers:
;eg. (sqrt (square .0005) results in .03125266401721204, two orders of magnitude off.

;It will also not work with large numbers because the number might overwhelm the memory so that the guess will never get within the tolerance provided because the computer cannot represent the number to that precision:
;eg. (sqrt 1e13) results in an infinite loop

(define (sqrt-iter guess old-guess x)
    (if (good-enough? guess old-guess)
        guess
        (sqrt-iter (improve guess x) guess x)))

(define (improve guess x)
    (average guess (/ x guess)))

(define (average x y)
    (/ (+ x y) 2))

(define (good-enough? guess old-guess)
    (< (abs (- (/ guess old-guess) 1)) 0.001))


(define (sqrt x)
    (sqrt-iter 1.0 2 x))

;(square (sqrt .0005)) => 5.000000002265612e-4
;(sqrt 1e13) => 3162277.6640104805