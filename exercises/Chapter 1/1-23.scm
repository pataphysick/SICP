#lang sicp

(define (square x) (* x x))

(define (timed-prime-test n)
    (newline)
    (display n)
    (start-prime-test n (runtime)))
(define (start-prime-test n start-time)
    (if (prime? n)
        (report-prime (- (runtime) start-time))))
(define (report-prime elapsed-time)
    (display " *** ")
    (display elapsed-time))

(define (smallest-divisor n) (find-divisor n 2))

(define (find-divisor n test-divisor)
    (cond ((> (square test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor n (next test-divisor)))))

(define (divides? a b) (= (remainder b a) 0))

(define (prime? n)
    (= n (smallest-divisor n)))

(define (next n)
    (if (= n 2)
        3
        (+ n 2)))

(define (search-for-primes start end)
    (define (iter n end)
        (timed-prime-test n)
        (if (< n (- end 1)) ;subtract 1 from end because otherwise the (+ n 2) will cause it to overshoot by 1
            (iter (+ n 2) end)))

    (if (= (remainder start 2) 0) ;if even, add one to start number before starting iteration so that it only tests odd numbers
        (iter (+ start 1) end)
        (iter start end)))

(search-for-primes 1e8 (+ 1e8 50))
;100000007.0 *** 1040
;100000037.0 *** 1000
;100000039.0 *** 999
;avg 1013
(search-for-primes 1e9 (+ 1e9 50))
;1000000007.0 *** 2999
;1000000009.0 *** 3019
;1000000021.0 *** 2998
;avg 3005
(search-for-primes 1e10 (+ 1e10 100))
;10000000019.0 *** 10997
;10000000033.0 *** 11993
;10000000061.0 *** 9014
;avg 10668
(search-for-primes 1e11 (+ 1e11 100))
;100000000003.0 *** 30989
;100000000019.0 *** 54963
;100000000057.0 *** 49983
;avg 45312

;2326/1013 = 2.30
;5672/3005 = 1.89
;30993/10668 = 2.91
;74989/45312 = 1.65
;The calculation time does appear (roughly) to have been halved.