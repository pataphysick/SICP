#lang sicp

(define (filtered-accumulate filter combiner null-value term a next b)
  (define (iter a result)
    (cond ((> a b) result)
          ((filter a) (iter (next a) (combiner result
                                           (term a))))
          (else (iter (next a) result))))
  (iter a null-value))

(define (sos-primes a b)
  (define (add x y) (+ x y))
  (define (inc x) (+ 1 x))
  (filtered-accumulate prime? add 0 square a inc b))

(define (prod-rel-prime n)
  (define (mult x y) (* x y))
  (define (identity x) x)
  (define (inc x) (+ x 1))
  (define (relprime? x)
    (= 1 (gcd x n)))
  (filtered-accumulate relprime? mult 1 identity 1 inc n))

(define (smallest-divisor n) (find-divisor n 2))
  (define (find-divisor n test-divisor)
    (cond ((> (square test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor n (+ test-divisor 1)))))
  (define (divides? a b) (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))

(define (square x) (* x x))