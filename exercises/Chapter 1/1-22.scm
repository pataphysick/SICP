#lang sicp

(define (timed-prime-test n)
    (newline)
    (display n)
    (start-prime-test n (runtime)))
(define (start-prime-test n start-time)
    (if (prime? n)
        (report-prime (- (runtime) start-time))))
(define (report-prime elapsed-time)
    (display " *** ")
    (display elapsed-time))

(define (smallest-divisor n) (find-divisor n 2))

(define (find-divisor n test-divisor)
    (cond ((> (square test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor n (+ test-divisor 1)))))

(define (divides? a b) (= (remainder b a) 0))

(define (prime? n)
    (= n (smallest-divisor n)))

(define (square x) (* x x))


(define (search-for-primes start end)
    (define (iter n end)
        (timed-prime-test n)
        (if (< n (- end 1)) ;subtract 1 from end because otherwise the (+ n 2) will cause it to overshoot by 1
            (iter (+ n 2) end)))

    (if (= (remainder start 2) 0) ;if even, add one to start number before starting iteration so that it only tests odd numbers
        (iter (+ start 1) end)
        (iter start end)))


;All the numbers suggested in book measured 0, likely due to the speed of computers increasing since the book was published. Had to start from 1e8 to get measurable numbers:
(search-for-primes 1e8 (+ 1e8 50))
;100000007.0 *** 2999
;100000037.0 *** 1981
;100000039.0 *** 1999
;avg 2326
(search-for-primes 1e9 (+ 1e9 50))
;1000000007.0 *** 6016
;1000000009.0 *** 4999
;1000000021.0 *** 6001
;avg 5672
(search-for-primes 1e10 (+ 1e10 100))
;10000000019.0 *** 44976
;10000000033.0 *** 16994
;10000000061.0 *** 31010
;avg 30993
(search-for-primes 1e11 (+ 1e11 100))
;100000000003.0 *** 79995
;100000000019.0 *** 67998
;100000000057.0 *** 76974
;avg 74989

;sqrt(10) = 3.16...
;5672/2326 = 2.44
;30993/5672 = 5.46
;74989/30993 = 2.42
;The numbers do grow at a rate very roughly around sqrt(10), but it's hard to tell without more samples.