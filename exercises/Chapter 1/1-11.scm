;recursive process:
(define (f n)
    (if (< n 3) n
    (+ (f (- n 1)) 
       (* 2 (f (- n 2))) 
       (* 3 (f (- n 3))))))

;iterative process:
(define (f n)
    (define rem (- n (floor n)))
    (define (iter count fn-1 fn-2 fn-3)
        (if (< count 3)
        fn-1
        (iter (- count 1) 
              (+ fn-1 
                 (* 2 fn-2) 
                 (* 3 fn-3)) 
              fn-1 
              fn-2)))
    (iter n (+ 2 rem) (+ 1 rem) rem)) ;can use n 2 1 0 as intial parameters if you assume that n is an integer, but that wasn't explicitly stated in the problem