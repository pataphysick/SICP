#lang sicp
(#%require (lib "27.ss" "srfi"))

(define (fermat-test n)
    (define (try-it a)
        (= (expmod a n n) a))
    (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
    (cond ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder
          (square (expmod base (/ exp 2) m))
          m))
        (else
         (remainder
          (* base (expmod base (- exp 1) m))
          m))))

(define (square x)
  (* x x))

(define (timed-prime-test n)
    (newline)
    (display n)
    (start-prime-test n (runtime)))
(define (start-prime-test n start-time)
    (if (fast-prime? n 10)
        (report-prime (- (runtime) start-time))))
(define (report-prime elapsed-time)
    (display " *** ")
    (display elapsed-time))

(define (search-for-primes start end)
    (define (iter n end)
        (timed-prime-test n)
        (if (< n (- end 1)) ;subtract 1 from end because otherwise the (+ n 2) will cause it to overshoot by 1
            (iter (+ n 2) end)))

    (if (= (remainder start 2) 0) ;if even, add one to start number before starting iteration so that it only tests odd numbers
        (iter (+ start 1) end)
        (iter start end)))


(search-for-primes (floor 1e200) (floor (+ 1e200 50)))
;With  Θ(log(n)) growth, you would expect the time to increase linearly with each factor of ten increase, so all the numbers should be of the same order. However, I could not get it working, which was apparently due to limitations on the size of numbers; you have to go much higher than 1e11 to find measurable computation times with the Fermat method, but the implementation breaks down at that point.
