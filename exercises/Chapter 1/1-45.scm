#lang sicp

(define (compose f g)
  (lambda (x) (f (g x))))

(define (repeated f n)
  (define (iter i g)
    (if (>= i n)
        g
        (iter (+ i 1) (compose f g))))
  (iter 1 f))

(define tolerance 0.00001)
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2))
       tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (average-damp f)
  (define (average a b)
    (/ (+ a b) 2))
  (lambda (x) (average x (f x))))

(define (square x) (* x x))

(define (16root x)
  (fixed-point (average-damp (average-damp (average-damp (average-damp (lambda (y) (/ x (* y y y y y y y y y y y y y y y)))))))
               1.0))

;2, 3: 1 avg damp
;4, 5, 6, 7: 2 avg damps
;8, 9, 10, 11, 12, 13, 14, 15: 3 avg damps
;16: 4 avg damps
;=> for nth root, need floor of log base 2 of n number of avg damps

(define (fast-expt b n)
    (define (even? x)
        (= (remainder x 2) 0))
    (define (iter a b n)
        (cond ((= n 0) a)
              ((even? n) (iter a (square b) (/ n 2)))
              (else (iter (* a b) b (- n 1)))))
    (iter 1 b n))

(define (n-root x n)
  (fixed-point ((repeated
                 average-damp
                 (floor (/ (log n) (log 2))))
                (lambda (y) (/ x (fast-expt y (- n 1)))))
               1.0))