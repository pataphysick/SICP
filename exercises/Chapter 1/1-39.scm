#lang sicp

(define (cont-frac-iter n d k)
  (define (iter i result)
    (if (< i 1)
        result
        (iter (- i 1)
              (/ (n i)
                 (+ (d i)
                    result)))))
  (iter k (/ (n k) (d k))))

(define (tan-cf x k)
  (define (n i)
    (if (= i 1)
        x
        (- 0 (square x))))
  (define (d i)
    (- (* 2 i) 1))
  (cont-frac-iter n d k))

(define (square x) (* x x))

> (tan-cf 3.1415928 10)
1.46570503232685e-007
> (tan-cf (/ 3.1415928 4) 10)
1.0000000732051062
> (tan-cf (* 3.1415928 2) 10)
7.982969127835605e-006