#lang sicp

;recursive
(define (cont-frac n d k)
  (define (iter i)
    (if (> i k)
        (/ (n k) (d k))
        (/ (n i)
           (+ (d i)
              (iter (+ i 1))))))
  (iter 1))

;iterative
(define (cont-frac-iter n d k)
  (define (iter i result)
    (if (< i 1)
        result
        (iter (- i 1)
              (/ (n i)
                 (+ (d i)
                    result)))))
  (iter k (/ (n k) (d k))))
    

(define phiinv
  (cont-frac (lambda (i) 1.0)
                  (lambda (i) 1.0)
                  12))

phiinv
(/ 1 phiinv)

;phi = 1.6180...
;Need k = 12 to match to four decimal places:
;0.6180371352785146
;1.6180257510729614