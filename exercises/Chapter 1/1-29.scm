#lang sicp

(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (simpson f a b n)
  (define h (/ (- b a) n))

  (define (add-h x)
    (+ x h))

  (define (inc x) (+ 1 x))

  (define (y k)
    (define (coeff k)
      (cond ((or (= 0 k) (= n k)) 1.0)
            ((= 0 (remainder k 2)) 2.0)
            (else 4.0)))
    (* (coeff k)
       (f (+ a (* h k)))))
  
  (* (/ h 3.0)
     (sum y 0 inc n)))

(define (cube x) (* x x x))

> (simpson cube 0 1 100)
0.24999999999999992
> (simpson cube 0 1 1000)
0.2500000000000002