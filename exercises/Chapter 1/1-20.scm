(define (gcd a b)
    (if (= b 0)
        a
        (gcd b (remainder a b))))

; GCD(206,40) = GCD(40,6)
; = GCD(6,4)
; = GCD(4,2)
; = GCD(2,0)
; = 2

; applicative:
(gcd 206 40)
(gcd 40 (remainder 206 40))
(gcd 40 6) ;1
(gcd 6 (remainder 40 6))
(gcd 6 4) ;2
(gcd 4 (remainder 6 4))
(gcd 4 2) ;3
(gcd 2 (remainder 4 2))
(gcd 2 0) ;4
2

; normal
(gcd 206 40)
(gcd 40 (remainder 206 40))
(if (= (remainder 206 40) 0)
(if (= 6 0) ;1
(gcd (remainder 206 40) (remainder 40 (remainder 206 40)))
(if (= (remainder 40 (remainder 206 40)) 0)
(if (= (remainder 40 6) 0) ;2
(if (= 4 0) ;3
(gcd (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40))))
(if (= (remainder (remainder 206 40) (remainder 40 (remainder 206 40))) 0)
(if (= 2 0) ;7
(gcd (remainder (remainder 206 40) (remainder 40 (remainder 206 40))) (remainder (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40))))
(if (= (remainder (remainder 40 (remainder 206 40)) (remainder (remainder 206 40) (remainder 40 (remainder 206 40))) 0)
(if (= 0 0)) ;14
(remainder (remainder 206 40) (remainder 40 (remainder 206 40)))
2 ;18

