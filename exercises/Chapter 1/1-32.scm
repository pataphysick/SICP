#lang sicp

;recursive
(define (accumulate combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a)
                (accumulate combiner null-value term (next a) next b))))

;iterative
(define (accumulate-iter combiner null-value term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (combiner result
                                 (term a)))))
  (iter a null-value))
                   

(define (sum term a next b)
  (define (add x y) (+ x y))
  (accumulate-iter add 0 term a next b))

(define (product term a next b)
  (define (mult x y) (* x y))
  (accumulate mult 1 term a next b))

(define (sumint a b)
  (define (identity x) x)
  (define (inc x) (+ 1 x))
  (sum identity a inc b))

(define (prodint a b)
  (define (identity x) x)
  (define (inc x) (+ 1 x))
  (product identity a inc b))