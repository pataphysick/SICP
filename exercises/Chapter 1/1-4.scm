(define (a-plus-abs-b a b)
    ((if (> b 0) + -) a b))

;Calculates a plus the absolute value of b by testing whether b is greater than 0. If it is, it applies the operator + to a and b, i.e. (+ a b). If b is less than or equal to 0, it applies the operator minus to a and b, (- a b).