#lang sicp

;You can show that phi is a fixed point of the transformation by repeatedly applying it, getting the continued fraction 1+1/(1+1/(1+1/....., which can be proved to be equal to phi if you start from the fact (shown in 1.13) that phi is the limit of n->inf of Fib(n)/Fib(n+1) and then try to expand the Fibonacci terms out.

(define tolerance 0.00001)
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2))
       tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

> (fixed-point (lambda (x) (+ 1 (/ 1.0 x))) 1)
1.6180327868852458