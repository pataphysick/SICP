#lang sicp
;By explicitly multiplying expmod by itself Louis is making the computer perform the procedure twice, defeating the purpose of using succesive squaring in the first place. If he had used square, because of applicative-order evaluation it would have performed expmod only once and then multiplied it by itself, thus saving a lot of work.
