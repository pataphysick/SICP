#lang sicp

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))

(define (square x) (* x x))

(define (carmichael-test n)
  (define (iter a n x)
    (if (< a n)
        (iter (+ a 1) n (+ x (test a n)))
        (display x)))
  (define (test a n)
    (if (= (expmod a n n) a)
        0
        1))
  (iter 1 n 0))