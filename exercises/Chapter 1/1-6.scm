(define (new-if predicate then-clause else-clause)
  (cond (predicate then-clause)
        (else else-clause)))

(define (sqrt-iter guess x)
  (new-if (good-enough? guess x)
          guess
          (sqrt-iter (improve guess x) x)))

(define (improve guess x)
    (average guess (/ x guess)))

(define (average x y)
    (/ (+ x y) 2))

(define (good-enough? guess x)
    (< (abs (- (square guess) x)) 0.001))

;Conditionals like cond and if are special in LISP and the else case won't evaluate if one of the preceding predicates is true. However, wrapping the cond in a new function makes it an expression, and to evaluate it LISP will try to keep substituting in sqrt-iter until it reaches a primitive element, resulting in an infinite loop.