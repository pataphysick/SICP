(define (p) (p))

(define (test x y)
  (if (= x 0)
      0
      y))

(test 0 (p))

;In applicative-order evaluation the interpreter tries to evaluate the (p) in (test 0 (p)) but only gets back (test 0 (p)), resulting in an infinite loop. In normal-order evaluation, the interpreter expands the expression and then evaluates it, and since the (p) is behind an 'else' that never evaluates, the procedure returns 0 without going into an infinite loop.