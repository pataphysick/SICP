#lang sicp

(define (iterative-improve good-enough? improve)
  (define (iter guess)
    (define next (improve guess))
    (if (good-enough? guess)
        next
        (iter next)))
  (lambda (guess) (iter guess)))


(define tolerance 0.00001)
(define (fixed-point f first-guess)
  (define (close-enough? x)
    (< (abs (- x (f x)))
       tolerance))
  ((iterative-improve close-enough? f) first-guess))


(define (sqrt x)
  (define (square x) (* x x))
  (define (average x y) (/ (+ x y) 2))
  
  (define (good-enough? guess)
    (< (abs (- (square guess) x)) 0.001))
  
  (define (improve guess)
    (average guess (/ x guess)))

  ((iterative-improve good-enough? improve) 1.0))