#lang sicp

;recursive version
(define (product term a next b)
  (if (> a b)
      1
      (* (term a)
         (product term (next a) next b))))

(define (factorial n)
  (define (identity x) x)
  (define (inc x) (+ x 1))
  (product identity 1 inc n))

(define (wallis n) ;for convenience treat n as an index grouping the two factors with the same denominator together
  (define (inc x) (+ x 1))
  (define (f i)
    (/ (* 4.0 i (+ i 1)) ;each numerator is 2i(2i+2) = 4i(i+1)
       (square (+ (* 2.0 i) 1))))
  (product-iter f 1 inc n))

(define (square x) (* x x))

;iterative version
(define (product-iter term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (* result (term a)))))
  (iter a 1))

> (* 4 (wallis 10))
3.213784940293189
> (* 4 (wallis 100))
3.149378473168601
> (* 4 (wallis 1000))
3.1423773650938855
> (* 4 (wallis 10000))
3.1416711865345