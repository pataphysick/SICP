; Base case: n=0 -> (phi^n - psi^n)/sqrt(5) = (phi^0 - psi^0)/sqrt(5) = (1 - 1)/sqrt(5) = 0, which is indeed Fib(0)
; For Fib(n): Fib(n) = Fib(n-1) + Fib(n-2) = (phi^(n-1) - psi^(n-1))/sqrt(5) - (psi^(n-2) + psi^(n-2))/sqrt(5)
;   = phi^n/sqrt(5) * (phi^-1 + phi^-2) - psi^n/sqrt(5) * (psi^-1 + psi^-2)
;   phi = (1+sqrt(5))/2, psi = (1-sqrt(5))/2
;   => phi^n/sqrt(5) * (2/(1+sqrt(5)) + 2/(3+sqrt(5))) - psi^n/sqrt(5) * (2/(1-sqrt(5)) + 2/(3-sqrt(5)))
;   = phi^n/sqrt(5) * (6+2*sqrt(5)+2+2*sqrt(5))/((1+sqrt(5))*(3+sqrt(5))) - psi^n/sqrt(5) * (6-2*sqrt(5)+2-2*sqrt(5))/((1-sqrt(5))*(3-sqrt(5)))
;   = phi^n/sqrt(5) * (8+4*sqrt(5))/(3+4*sqrt(5)+5) - psi^n/sqrt(5) * (8-4*sqrt(5))/(3-4*sqrt(5)+5)
;   = phi^n/sqrt(5) * (8+4*sqrt(5))/(8+4*sqrt(5)) - psi^n/sqrt(5) * (8-4*sqrt(5))/(8-4*sqrt(5))
;   = phi^n/sqrt(5) - psi^n/sqrt(5)
;   = (phi^n - psi^n)/sqrt(5)

; Proving that Fib(n) is the closest integer to phi^n/sqrt(5) is similar. In the base case, (phi^0)/sqrt(5) = 0.447..., so Fib(0) = 0 is the closest
; For the nth case, it has already been proven that Fib(n) = (phi^n - psi^n)/sqrt(5)
;   The next step is to notice that psi/sqrt(5) = (1-sqrt(5))/2 = -0.276...
;   That is, for n=1 (and in fact n=0 too) the magnitude of the psi term is already less than 0.5, and as n increases the psi term will only grow smaller and smaller while the phi term gets closer and closer to Fib(n)
;   Thus there can be no integer closer to phi^n/sqrt(5) than Fib(n) = (phi^n - psi^n)/sqrt(5) for any n