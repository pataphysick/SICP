#lang sicp

; Yes, Eva is right because having uncertain quantities occur in the formula multiple times will compound the uncertainty so that the interval widens, as seen in 2.14.