#lang sicp

; tolerance t = w/c = (ub - lb)/2c
; c = (ub + lb)/2 => ub = 2c - lb, lb = 2c - lb
; substitute into tolerance eq: t = ((2c - lb) - lb)/2c = (ub - (2c - ub))/2c
; => ub = c(t + 1), lb = c(1 - t)
; t_xy = (ub_x * ub_y - lb_x * lb_y)/(ub_x * ub_y + lb_x * lb_y)
; = (c_x * c_y * (t_x + 1)(t_y + 1) - c_x * c_y * (1 - t_x)(1 - t_y))/(c_x * c_y * (t_x + 1)(t_y + 1) + c_x * c_y * (1 - t_x)(1 - t_y))
; cancel c_x*c_y, multiply out: t_xy = (t_x * t_y + t_x + t_y + 1 - 1 + t_x + t_y - t_x * t_y)/(t_x * t_y + t_x + t_y + 1 + 1 - t_x - t_y + t_x * t_y)
; = 2(t_x + t_y)/2(t_x * t_y + 1)
; if t_x and t_y are small, t_x * t_y is negligible and the equation reduces to t_xy = t_x + t_y