#lang sicp

;Louis' procedure calls queen-cols for every column-row combination instead of once for every column. For the 8x8 case, this would make it about T^8 times slower.