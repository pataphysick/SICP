#lang sicp

(define (element-of-set? x set)
  (cond ((null? set) false)
        ((equal? x (car set)) true)
        (else (element-of-set? x (cdr set)))))

(define (union-set set1 set2)
  (define (iter set union)
    (cond ((null? set) union)
          ((not (element-of-set? (car set) union))
           (iter (cdr set)
                 (cons (car set) union)))
          (else (iter (cdr set) union))))
  (iter (append set1 set2) '()))