#lang sicp
(#%require sicp-pict)

(define (below painter1 painter2)
  (let ((split-point (make-vect 0.0 0.5)))
    (let ((bottom (transform-painter painter1
                                     (make-vect 0.0 0.0)
                                     (make-vect 1.0 0.0)
                                     split-point))
          (top (transform-painter painter2
                                  split-point
                                  (make-vect 1.0 0.5)
                                  (make-vect 0.0 1.0))))
      (lambda (frame)
        (bottom frame)
        (top frame)))))


(define (below painter1 painter2)
  (rotate270 (beside (rotate90 painte2)
                     (rotate90 painter1))))