#lang sicp

(define (cons a b)
  (* (expt 2 a)
     (expt 3 b)))

(define (factor n k)
  (define (iter m i)
    (if (not (= 0 (remainder m k)))
        i
        (iter (/ m k) (+ i 1))))
  (iter n 0))

(define (car x)
  (factor x 2))

(define (cdr x)
  (factor x 3))