(define (equ? x y)
  (apply-generic 'equ? x y))

(put 'equ? '(scheme-number scheme-number)
     (lambda (x y) (attach-tag 'scheme-number
			       (= x y))))

(put 'equ? '(rational rational)
     (lambda (x y) (attach-tag 'rational
			       (and (= (numer x) (numer y))
				    (= (denom x) (denom y))))))

(put 'equ? '(complex complex)
     (lambda (x y) (attach-tag 'complex
			       (and (= (real-part x) (real-part y))
				    (= (imag-part x) (imag-part y))))))
