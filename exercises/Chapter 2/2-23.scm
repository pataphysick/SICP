#lang sicp

(define (for-each proc list)
  (cond ((null? list) true)
        (else (proc (car list))
              (for-each proc (cdr list)))))