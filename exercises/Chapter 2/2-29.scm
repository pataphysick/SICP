#lang sicp

(define (make-mobile left right)
  (list left right))

(define (make-branch length structure)
  (list length structure))

(define (left-branch mobile)
  (car mobile))

(define (right-branch mobile)
  (car (cdr mobile)))

(define (branch-length branch)
  (car branch))

(define (branch-structure branch)
  (car (cdr branch)))

(define (total-weight m)
  (cond ((null? m) 0)
        ((not (pair? m)) m)
        (else (+ (total-weight (branch-structure (left-branch m)))
                 (total-weight (branch-structure (right-branch m)))))))

(define (balanced? m)
    (if (not (pair? m))
        true
        (let ((l (left-branch m)) ;had to put let statement inside conditional b/c otherwise it tries to access the branches of even the terminal weights and throws an error
              (r (right-branch m)))
          (if (not (= (* (branch-length l) (total-weight (branch-structure l)))
                      (* (branch-length r) (total-weight (branch-structure r)))))
              false
              (and (balanced? (branch-structure l))
                   (balanced? (branch-structure r)))))))