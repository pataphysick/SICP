(define (attach-tag type-tag contents)
  (if (eq? type-tag 'scheme-number) ;thanks to people on the internet for correction of the bug created by just using (number? contents), which was my solution
    contents
    (cons type-tag contents)))

(define (type-tag datum)
  (cond ((pair? datum) (car datum))
	((number? datum) 'scheme-number)
	(else (error "Bad tagged datum: type-tag" datum))))

(define (contents datum)
  (cond ((pair? datum) (cdr datum))
	((number? datum) datum)
	(else (error "Bad tagged datum: contents" datum))))
