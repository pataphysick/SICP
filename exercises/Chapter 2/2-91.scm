(define (div-terms L1 L2)
  (if (empty-termlist? L1)
      (list (the-empty-termlist) (the-empty-termlist))
      (let ((t1 (first-term L1))
            (t2 (first-term L2)))
        (if (> (order t2) (order t1))
            (list (the-empty-termlist) L1)
            (let ((new-c (div (coeff t1) (coeff t2)))
                  (new-o (- (order t1) (order t2))))
              (let ((rest-of-result
                     (div-terms (sub-terms L1 
                                           (mul-term-by-all-terms (make-term new-o new-c)
                                                                  L2))
                                L2)))
                (list (adjoin-term (make-term new-o new-c)
                                   (car rest-of-result))
                      (cadr rest-of-result))))))))

; Quick and dirty definition of sub-terms since I went straight to div-poly in 2.88
(define (negate-terms terms)
  (let loop ((a terms)
	     (b (the-empty-termlist)))
    (if (empty-termlist? a)
        b
      	(loop (rest-terms a)
	      (adjoin-term (negate (first-term a)) b)))))

(define (sub-terms t1 t2)
  (add-terms t1
	     (negate-terms t2)))


(define (div-poly p1 p2)
  (if (same-variable? (variable p1) (variable p2))
      (let ((divided (div-terms (term-list p1) (term-list p2))))
        (list (make-poly (variable p1)
                         (car divided))
              (make-poly (variable p1)
                         (cadr divided))))
      (error "Polys not in same var: div-poly" (list p1 p2))))
