#lang sicp

;a. The dispatch uses the operation to be performed as the type. Pure numbers and variables can't be subsumed because they are the base cases, and do not have an operation.

;b.

(define (install-deriv-package)

  (define (addend exp)
    (cadr exp))
  (define (augend exp)
    (caadr exp))

  (define (make-sum x y)
    (list '+ x y))
  
  (define (deriv-sum exp var)
    (make-sum (deriv (addend exp) var)
              (deriv (augend exp) var)))

  (put 'deriv '+ deriv-sum)

  (define (multiplicand exp)
    (cadr exp))
  (define (multiplier exp)
    (caddr exp))

  (define (make-prod x y)
    (list '* x y))


  (define (deriv-prod exp var)
    (make-sum (make-prod (deriv (multiplicand exp) var)
                         (multiplier exp))
              (make-prod (deriv (multiplier exp) var)
                         (multiplicand exp))))

  (put 'deriv '* deriv-prod)
  
  'done)

;c.

(define (deriv-exp exp var)
  (make-prod (make-prod (exponent exp)
                        (make-exponentiation (base exp)
                                             (make-sum (exponent exp) -1)))
             (deriv (base exxp) var)))

(put 'deriv '** deriv-exp)

;d.
;Just install them in the new order like so:
(put '+ 'deriv deriv-sum)