(define (gcd-terms a b)
  (if (empty-termlist? b)
    a
    (gcd-terms b (remainder-terms a b))))

(define (remainder-terms t1 t2)
  (cadr (div-terms t1 t2)))

(define (gcd-poly p1 p2)
  (if (same-variable? (variable p1) (variable p2))
      (make-poly (variable p1)
		 (gcd-terms (term-list p1)
			    (term-list p2)))
      (error "polys not in same var: gcd-poly" (list p1 p2))))

(put 'greatest-common-divisor '(scheme-number scheme-number)
     (lambda (m n) (gcd m n)))

(put 'greatest-common-divisor '(polynomial polynomial)
     (lambda (p1 p2) (gcd-poly p1 p2)))
