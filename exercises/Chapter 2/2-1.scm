#lang sicp


(define (make-rat n d)
  (define (neg x) (- 0 x))
  (cond ((and (< n 0) (< d 0))
         (make-rat (neg n) (neg d)))
        ((< d 0)
         (make-rat (neg n) (neg d)))
        (else
         (let ((g (gcd n d)))
                 (cons (/ n g) (/ d g))))))