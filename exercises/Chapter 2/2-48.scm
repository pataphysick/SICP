#lang sicp

(define (make-vect x y)
  (cons x y))

(define (xcor-vect v)
  (car v))

(define (ycor-vect v)
  (cdr v))

(define (make-segment v w)
  (cons v w))

(define (start-segment s)
  (car s))

(define (end-segment s)
  (cdr s))