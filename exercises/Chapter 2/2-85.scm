(define (project-complex z)
  (make-real (real-part z)))

(define (project-real r)
  (make-rat (rationalize->exact r 0)))

(define (project-rat r)
  (make-int (round r)))

(put 'project 'complex project-complex)
(put 'project 'real project-real)
(put 'project 'rational project-rat)


(define (drop num)
  (let ((projected ((get 'project (type-tag num)) 
		    num))
	(let ((raised ((get 'raise (type-tag projected)) 
		       projected))
	      (if (equ? raised num)
		(drop projected)
		num))))))

(define (apply-generic op . args)
  (let ((arg1 (car args))
	(arg2 (cadr args)))
    (let ((type1 (type-tag arg1))
	  (type2 (type-tag arg2)))
      (if (equal? type1 type2)
	(apply op args)
	(let ((raised1 (successive-raise arg1 type2)))
	  (if raised1
	    (drop (op raised1 arg2))
	    (let ((raised2 (successive-raise arg2 type1)))
	      (if raised2
		(drop (op arg1 raised2))
		(error "No method for these types")))))))))

(define (successive-raise arg to-type)
  (let ((from-type (type-tag arg)))
    (let ((proc (get 'raise from-type)))
      (cond ((not proc) #f)
	    ((equal? from-type to-type) arg)
	    (else (successive-raise (proc arg) to-type))))))

