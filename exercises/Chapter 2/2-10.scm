#lang sicp

(define (make-interval a b) (cons a b))

(define (lower-bound i)
  (let ((a (car i))
        (b (cdr i)))
    (min a b)))

(define (upper-bound i)
  (let ((a (car i))
        (b (cdr i)))
    (max a b)))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (let ((u (upper-bound y))
        (l (lower-bound y)))
    (if (or (and (negative? u) (positive? l))
            (and (positive? u) (negative? l)))
        (error "Interval spans zero")
        (mul-interval
         x
         (make-interval (/ 1.0 u)
                        (/ 1.0 l))))))
