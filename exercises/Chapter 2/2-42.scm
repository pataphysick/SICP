#lang sicp

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (flatmap proc seq)
  (accumulate append nil (map proc seq)))

(define (enumerate-interval low high)
  (if (> low high)
      nil
      (cons low (enumerate-interval (+ low 1) high))))

(define (filter predicate sequence)
  (cond ((null? sequence) nil)
        ((predicate (car sequence))
         (cons (car sequence)
               (filter predicate (cdr sequence))))
        (else (filter predicate (cdr sequence)))))

(define (queens board-size)
  (define (queen-cols k)
    (if (= k 0)
        (list empty-board)
        (filter
         (lambda (positions) (safe? k positions))
         (flatmap
          (lambda (rest-of-queens)
            (map (lambda (new-row)
                   (adjoin-position
                    new-row k rest-of-queens))
                 (enumerate-interval 1 board-size)))
          (queen-cols (- k 1))))))
  (queen-cols board-size))

(define (set-position row col)
  (cons row col))

(define (get-row position)
  (car position))

(define (get-col position)
  (cdr position))

(define empty-board nil)

(define (adjoin-position row col rest)
  (append rest (list (set-position row col))))

(define (safe? k positions)
  (if (null? positions)
      true
      (let ((row (get-row (car (filter (lambda (x) (= k (get-col x))) positions)))) ;get row corresponding to column k from list of positions
            (rest (filter (lambda (x) (not (= k (get-col x)))) positions))) ;remove queen in col k from list of positions
        (accumulate (lambda (x y) (and x y)) ;and together the results of checking compatibility of queen in col k with each of the others
                    true
                    (map (lambda (queen)
                           (cond ((= row (get-row queen)) false) ;false if col k queen in same row as other queen
                                 ((= (abs (- k (get-col queen))) (abs (- row (get-row queen)))) false) ;false if col k queen diagonal to other queen
                                 (else true)))
                         rest)))))