(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((to-type (get-coercion-type op type-tags)))
      (accum op args to-type))))

(define (get-coercion-type op type-tags)
  (if ((null? type-tags)
       (error "No method for these types"
	      (list op type-tags)))
    (let ((current-type (car type-tags))
	  (other-types (cdr type-tags)))
      (if (check other-types current-type)
	current-type
	(get-coercion-type other-types)))))

(define (check type-tags current-type)
  (if (null? type-tags)
    #t
    (let ((proc (get-coercion (car type-tags) current-type)))
      (if (or proc (equal? current-type (car type-tags)))
	(check (cdr type-tags))
	#f))))

(define (accum op args to-type)

  (define (coerce arg)
    (let ((from-type (type-tag arg)))
      (if (equal? from-type to-type)
	arg
	(apply (get-coercion from-type to-type) arg))))

  (define (iter cum lst)
    (if (null? lst)
      cum
      (iter (op cum 
		(coerce (car lst)))
	    (cdr lst))))

  (iter (coerce (car args)) (cdr args)))

; This is not sufficiently general when the types of the arguments cannot be coerced into each other but can all be coerced into another type.
