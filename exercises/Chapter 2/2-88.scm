(define (negate-scheme-number n)
  (sub (make-scheme-number 0) n))

(define (negate-rat r)
  (sub (make-rat 0 1) r))

(define (negate-complex z)
  (make-from-real-imag (- 0 (real-part z))
		       (- 0 (imag-part z))))

(define (negate-polynomial p)
  (let ((terms (term-list p)))
    (let loop ((a terms)
               (b (the-empty-termlist)))
      (if (empty-termlist? a)
          b
	  (loop (rest-terms a)
		(adjoin-term (negate (first-term a)) b))))))

;install in corresponding packages
(put 'negate 'scheme-number
     (lambda (n) (tag (negate-scheme-number n))))

(put 'negate 'rational
     (lambda (r) (tag (negate-rat r))))

(put 'negate 'complex
     (lambda (z) (tag (negate-complex z))))

(put 'negate 'polynomial
     (lambda (p) (tag (negate-polynomial p))))

;define generic negate
(define (negate x) (apply-generic 'negate x))


;define and install sub for polynomials
(put 'sub '(polynomial polynomial)
     (lambda (p1 p2) (tag (add-poly p1 (negate p2)))))
