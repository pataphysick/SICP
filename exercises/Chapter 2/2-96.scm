; a
(define (pseudoremainder-terms t1 t2)
  (let ((o1 (order (first-term t1)))
	(o2 (order (first-term t2)))
	(c (coeff (first-term t2))))
    (cadr (div-terms (mul-term-by-all-terms (make-term 0 (expt c (sub (add 1 o1) o2)))
					    t1)
		     t2))))

(define (gcd-terms a b)
  (if (empty-termlist? b)
    a
    (gcd-terms b (pseudoremainder-terms a b))))

(define (gcd-poly p1 p2)
  (if (same-variable? (variable p1) (variable p2))
      (make-poly (variable p1)
		 (gcd-terms (term-list p1)
			    (term-list p2)))
      (error "polys not in same var: gcd-poly" (list p1 p2))))

; b
(define (gcd-all t)
    (fold-right (lambda (a b)
		  (gcd a (coeff b)))
		0
		t))

(define (gcd-terms t1 t2)
  (if (empty-termlist? t2)
      (let ((divisor (gcd-all t1)))
	(map (lambda (a) 
	       (div (coeff a) divisor))
	     t1))
      (gcd-terms t2 (pseudoremainder-terms t1 t2))))
