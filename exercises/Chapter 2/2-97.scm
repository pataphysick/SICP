; a

(define (reduce-terms n d)
  (let* ((terms-gcd (gcd-terms n d))
	 (c (coeff (first-term terms-gcd)))
	 (o2 (order (first-term terms-gcd)))
	 (o1 (max (order (first-term n))
		  (order (first-term d))))
	 (factor (expt c (add (sub o1 o2) 1)))
	 (n2 (mul n factor))
	 (d2 (mul d factor))
	 (divisor (gcd (gcd-all n2)
		       (gcd-all d2))))
    (list (div-terms n2 divisor)
	  (div-terms d2 divisor))))

(define (reduce-poly p1 p2)
  (if (same-variable? (variable p1) (variable p2))
    (let ((var (variable p1)
	       (reduced (reduce-terms (term-list p1) (term-list p2)))))
      (list (make-poly var (car reduced))
	    (make-poly var (cadr reduced))))
    (error "Polys not in same var: reduce-poly" (list p1 p2))))

; b

(define (reduce-integers n d)
  (let ((g (gcd n d)))
    (list (/ n g) (/ d g))))

; assuming adding with rest of package and thus uses the right tag operation for their types
(put 'reduce '(scheme-number scheme-number)
     (lambda (n d) (map tag (reduce-integers n d))))

(put 'reduce '(polynomial polynomial)
     (lambda (n d) (map tag (reduce-poly n d))))
