#lang sicp

; As mentioned before, since we are working not with exact numbers but with ranges, the precise way in which the quantities are combined becomes important because applying the range multiple times makes the interval of the result widen.
; For instance, if you had a range (2, 3) and you just kept multiplying it by itself, it would get larger and larger, growing to (4, 9), (8, 27), (16, 81), etc.
; I assume the latter part of the exercise is cheeky textbook speak for "this is an unsolved problem in the field and many people a lot smarter than you haven't been able to do it".
