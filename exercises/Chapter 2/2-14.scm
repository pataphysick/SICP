#lang sicp

(define (make-interval a b) (cons a b))

(define (lower-bound i)
  (let ((a (car i))
        (b (cdr i)))
    (min a b)))

(define (upper-bound i)
  (let ((a (car i))
        (b (cdr i)))
    (max a b)))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (mul-interval
   x
   (make-interval (/ 1.0 (upper-bound y))
                  (/ 1.0 (lower-bound y)))))

(define (make-center-percent c p)
  (let ((w (* c (/ p 100))))
    (make-interval (- c w) (+ c w))))

(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))

(define (percent i)
  (let ((c (center i))
        (w (- (upper-bound i) (lower-bound i))))
    (* 100 (/ w c))))

(define (par1 r1 r2)
  (div-interval (mul-interval r1 r2)
                (add-interval r1 r2)))

(define (par2 r1 r2)
  (let ((one (make-interval 1 1)))
    (div-interval
     one (add-interval (div-interval one r1)
                       (div-interval one r2)))))

> (define r1 (make-center-percent 1 1))
> (define r2 (make-center-percent 2 1))
> (par1 r1 r2)
(0.646930693069307 . 0.6869360269360268)
> (par2 r1 r2)
(0.66 . 0.6733333333333333)

> (define r1 (make-center-percent 1 .5))
> (define r2 (make-center-percent -1 1))
> (par1 r1 r2)
(-67.67000000000044 . 67.66999999999995)
> (par2 r1 r2)
(-66.3300000000003 . 66.99666666666703)

> (define r1 (make-center-percent 100 .1))
> (define r2 (make-center-percent 100 .5))
> (par1 r1 r2)
(49.55159521435694 . 50.45160481444332)
> (par2 r1 r2)
(49.84979939819459 . 50.14980059820539)

> (define r1 (make-center-percent -5 .0001))
> (define r2 (make-center-percent -10 .00001))
> (par1 r1 r2)
(-3.3333383333356656 . -3.3333283333356674)
> (par2 r1 r2)
(-3.3333356666660663 . -3.3333309999994)

;the first equation produces wider intervals than the second