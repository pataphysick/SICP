#lang sicp

(define (make-interval a b) (cons a b))

(define (lower-bound i)
  (let ((a (car i))
        (b (cdr i)))
    (min a b)))

(define (upper-bound i)
  (let ((a (car i))
        (b (cdr i)))
    (max a b)))

;possible combinations of sign of xlb/ylb/xub/yub along with the combos that produce the lowest and highest bounds:
; ---- (xub yub)(xlb ylb)
; -+-+ (xlb yub)(xub ylb)
; +-+- (xub ylb)(xlb yub)
; ++++ (xlb ylb)(xub yub)
; --++ (min (xlb yub) (xub ylb))(max (xlb ylb) (xub yub))
; ---+ (xlb yub)(xlb ylb)
; --+- (xub ylb)(xlb ylb)
; -+++ (xlb yub)(xub yub)
; +-++ (xub ylb)(xub yub)

(define (mul-interval x y)
  (let ((xlb (lower-bound x))
        (ylb (lower-bound y))
        (xub (upper-bound x))
        (yub (upper-bound y)))
    (cond ((and (positive? xlb) (positive? ylb) (positive? xub) (positive? yub))
           (make-interval (* xlb ylb) (* xub yub)))
          ((and (negative? xlb) (positive? ylb) (positive? xub) (positive? yub))
           (make-interval (* xlb yub) (* xub yub)))
          ((and (positive? xlb) (negative? ylb) (positive? xub) (positive? yub))
           (make-interval (* xub ylb) (* xub yub)))
          ((and (negative? xlb) (positive? ylb) (negative? xub) (positive? yub))
           (make-interval (* xlb yub) (* xub ylb)))
          ((and (positive? xlb) (negative? ylb) (positive? xub) (negative? yub))
           (make-interval (* xub ylb) (* xlb yub)))
          ((and (negative? xlb) (negative? ylb) (negative? xub) (positive? yub))
           (make-interval (* xlb yub) (* xlb ylb)))
          ((and (negative? xlb) (negative? ylb) (positive? xub) (negative? yub))
           (make-interval (* xub ylb) (* xlb ylb)))
          ((and (negative? xlb) (negative? ylb) (negative? xub) (negative? yub))
           (make-interval (* xub yub) (* xlb ylb)))
          (else
           (make-interval (min (* xlb yub) (* xub ylb))
                          (max (* xlb ylb) (* xub yub)))))))

          
          