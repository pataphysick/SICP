(define (=zero? x) (apply-generic '=zero? x))

(put '=zero? 'scheme-number
     (lambda (x) (attach-tag 'scheme-number
			     (= 0 x))))

(put '=zero? 'rational
     (lambda (x) (attach-tag 'rational
			     (and (= 0 (numer x))
				  (not (= 0 (denom x)))))))

(put '=zero? 'complex
     (lambda (x) (attach-tag 'complex
			     (and (= 0 (real-part x))
				  (= 0 (imag-part x))))))
				  
