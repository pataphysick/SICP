#lang sicp

(define (adjoin-set s x)
  (cond ((null? s) (list x))
        ((eq? (car s) x) s)
        ((> (car s) x) (cons x s))
        (else (cons (car s) (adjoin-set (cdr s) x)))))