#lang sicp

(define (element-of-set? x set)
  (cond ((null? set) false)
        ((equal? x (car set)) true)
        (else (element-of-set? x (cdr set)))))

(define (adjoin-set x set)
  (cons x set))

(define (intersection-set set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of-set? (car set1) set2)
         (cons (car set1) (intersection-set (cdr set1) set2)))
        (else (intersection-set (cdr set1) set2))))

(define (union-set set1 set2)
  (append set1 set2))

;The efficiency of element-of-set? varies depending on the set. Because the sets can be larger it could be more inefficient, but allowing multiples might also make it more efficient because it might encounter a match earlier on in the set.
;The efficiencies of adjoin-set and union-set are much greater because they simply add to the sets without checking if there are duplicates.
;The efficiency of intersection-set will be worse since, although the procedure is exactly the same, it will have to traverse larger sets and repeat itself for each instance of an element.
;Of course, this is time efficiency. The space efficiency will universally be worse.
;This representaton might be useful in cases in which the count of each element is important. For instance, in making a histogram, especially if you have a discrete variable that naturally falls into a only a few values, would require retaining the repeats.