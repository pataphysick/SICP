#lang sicp

;Solution from the Scheme Wiki. I kept running into the issue of cons-ing nil with something else, but this elegantly sidesteps the issue by starting from the right instead of the left.
(define (fringe list)
  (define (iter list1 list2)
    (cond ((null? list1) list2)
          ((not (pair? list1)) (cons list1 list2))
          (else (iter (car list1)
                      (iter (cdr list1) list2)))))
  (iter list nil))
