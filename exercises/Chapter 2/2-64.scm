#lang sicp

(define (list->tree elements)
  (car (partial-tree elements (length elements))))
(define (partial-tree elts n)
  (if (= n 0)
      (cons '() elts)
      (let ((left-size (quotient (- n 1) 2)))
        (let ((left-result
               (partial-tree elts left-size)))
          (let ((left-tree (car left-result))
                (non-left-elts (cdr left-result))
                (right-size (- n (+ left-size 1))))
            (let ((this-entry (car non-left-elts))
                  (right-result
                   (partial-tree
                    (cdr non-left-elts)
                    right-size)))
              (let ((right-tree (car right-result))
                    (remaining-elts
                     (cdr right-result)))
                (cons (make-tree this-entry
                                 left-tree
                                 right-tree)
                      remaining-elts))))))))

(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
(list entry left right))

;a. partial-tree first calculates the number of elements to be in the left half of the tree, and builds that tree recursively. It then uses the remaining elements to get the element at the current node and the number of elements to be in the right half of the tree, which it builds in the same manner as the left tree. Then it makes a list out of these three. partial-tree is called recursively until it is called to make a tree with 0 elements, at which point it returns an empty list for the tree, along with all the remaing elements to be added to a different branch.
; The tree produced for (1 3 5 7 9 11) is:
;    5
;   / \
;  /   \
; 1     9
;  \   / \
;   3 7  11
;The left branch of the 1 node and both branches coming from the 3, 7, and 11 nodes are all empty lists.

;b. partial-tree is called once for every element in the list, so its order of growth will be Θ(n)