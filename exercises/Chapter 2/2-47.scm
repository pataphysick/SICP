#lang sicp

(define (make-frame origin edge1 edge2)
  (list origin edge1 edge2))

(define (make-frame2 origin edge1 edge2)
  (cons origin (cons edge1 edge2)))

(define (origin-frame f)
  (car f))

(define (edge1-frame f)
  (cadr f))

(define (edge2-frame f)
  (caddr f))

(define (origin-frame2 f)
  (car f))

(define (edge1-frame2 f)
  (cadr f))

(define (edge2-frame2 f)
  (cddr f))