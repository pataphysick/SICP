#lang sicp

;a
(define outline
  (let ((a (make-vect 0 0))
        (b (make-vect 0 1))
        (c (make-vect 1 0))
        (d (make-vect 1 1)))
    (segments->painter
     (list (make-segment a b)
           (make-segment a c)
           (make-segment b d)
           (make-segment c d)))))

;b
(define x
  (let ((a (make-vect 0 0))
        (b (make-vect 0 1))
        (c (make-vect 1 0))
        (d (make-vect 1 1)))
    (segments->painter
     (list (make-segment a d)
           (make-segment b c)))))

;c
(define diamond
  (let ((a (make-vect .5 0))
        (b (make-vect 0 .5))
        (c (make-vect 1 .5))
        (d (make-vect .5 1)))
    (segments->painter
     (list (make-segment a b)
           (make-segment a c)
           (make-segment b d)
           (make-segment c d)))))

;d
;I would prefer not to.