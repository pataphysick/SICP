#lang sicp
(#%require sicp-pict)

(define (split proc1 proc2)
  (define (splat painter n)
    (if (= n 0)
        painter
        (let ((smaller (splat painter (- n 1))))
          (proc1 painter (proc2 smaller smaller)))))
  (lambda (painter n) (splat painter n)))