#lang sicp

(define (make-interval a b) (cons a b))

(define (lower-bound i)
  (let ((a (car i))
        (b (cdr i)))
    (min a b)))

(define (upper-bound i)
  (let ((a (car i))
        (b (cdr i)))
    (max a b)))

(define (sub-interval x y)
  (make-interval (- (lower-bound x) (upper-bound y))
                 (- (upper-bound x) (lower-bound y))))