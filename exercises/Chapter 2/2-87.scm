(define (terms-zero? terms)
  (cond ((empty-termlist? terms) #t)
	((not (=zero? (coeff (first-term terms)))) #f)
	(else (terms-zero? (rest-terms terms)))))

(put '=zero? 'polynomial
     (lambda (p)
       (terms-zero? (term-list p))))
