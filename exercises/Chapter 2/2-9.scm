#lang sicp

;For addition/subtraction:
; w = (ub + lb)/2 => ub = 2w + lb, lb = ub - 2w
; ub_x+y = ub_x + ub_y, lb_x+y = lb_x + lb_y
; => ub_x+y = 2w_x + lb_x + 2w_y + lb_y, lb_x+y = ub_x - 2w_x + ub_y - 2w_y
; => w_x+y = (2w_x + lb_x + 2w_y + lb_y - ub_x + 2w_x - ub_y + 2w_y)/2
; = 2w_x + 2w_y + (lb_x-ub_x)/2 + (lb_y - ub_y)/2 = 2w_x + 2w_y - w_x - w_y = w_x + w_y
; => w_x+y = w_x + w_y


;For multiplication/division:
> (define x (make-interval 2 3)) ; width 0.5
(define y (make-interval 1 4)) ; width 1.5
(width (mul-interval x y))
5.0
> (define x (make-interval 3 4)) ;width 0.5
(define y (make-interval 2 5)) ;width 1.5
(width (mul-interval x y))
7.0
; As can be seen, the width of the interval resulting from multiplication or division of two intervals is related to the bounds of the intervals, not their widths.