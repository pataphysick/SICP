Using explicit dispatch, adding new data types requires that the old procedures be rewritten to account for the new data type. Adding new operations is simpler but requires that no two procedures have the same name even if they perform the same operationn but on different data types.

Using data-directed programming, adding new types requires writing new operations for that type, and adding new operations requires writing that procedure for each type. In both cases the new procedures must then be installed in the system using the put procedure.

In message passing, adding procedures and data types is the same: the procedure defining the new data type actually just defines how to perform the possible operations on that data type. Thus adding data types requires writing a new such procedure, and adding operations requires modifying all the procedures defining the old data types to add in the new operation.

Message passing style would more convenient for a situation in which new data types are often added, and data-directed style would be appropriate for one in which new operations are added.