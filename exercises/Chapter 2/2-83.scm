(define (raise-int i)
  (make-rat i 1))

(define (raise-rat r)
  (make-real (/ (numer r) (denom r) 1.0)))

(define (raise-real real)
  (make-from-real-imag real 0))

(put 'raise 'scheme-number raise-int)
(put 'raise 'rational raise-rat)
(put 'raise 'real raise-real)
