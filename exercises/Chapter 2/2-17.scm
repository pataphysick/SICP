#lang sicp

(define (last-pair list)
  (define (iter l n)
    (if (null? l)
        n
        (iter (cdr l) (car l))))
  (iter list 0))