#lang sicp

;a. The procedures do both give the same answer, the list (1 3 5 7 9 11).
;b. The second procedure grows more slowly since it takes an iterative approach and doesn't have to use append, a procedure that goes through the whole list, at every step.