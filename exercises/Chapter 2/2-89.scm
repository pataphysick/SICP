(define (dense-adjoin-term term term-list)
  (if (=zero? term)
    term-list
    (if (= (length term-list) (order term)) ; i.e. if the order of the term is one greater than the current highest order in the term list
      	(cons (coeff term) term-list)
	(dense-adjoin-term term (cons 0 term-list)))))

(define (dense-first-term term-list)
  (make-term (- (length term-list) 1)
	     (car term-list)))
