#lang sicp

;#1:
(car (cdr (car (cdr (cdr list)))))

;#2:
(car (car list))

;#3:
(car (cdr (car (cdr (car (cdr (car (cdr (car (cdr (car (cdr list))))))))))))