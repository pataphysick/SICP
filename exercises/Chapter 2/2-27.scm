#lang sicp


(define (deep-reverse list)
  (define (iter list1 list2)
    (cond ((null? list1) list2)
          ((not (pair? list1)) list1)
          (else (iter (cdr list1)
                      (cons (deep-reverse (car list1))
                            list2)))))
  (iter list nil))
