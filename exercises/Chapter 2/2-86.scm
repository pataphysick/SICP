;; We need to define and install cosine, sine, atan, square, and sqrt for the scheme-number, rational, and real packages:
(put 'cos 'scheme-number 
     (lambda (x) ((get 'tag 'real)
		  (make-real (cos x)))))
(put 'sin 'scheme-number 
     (lambda (x) ((get 'tag 'real)
		  (make-real (sin x)))))
(put 'atan '(scheme-number scheme-number)
     (lambda (x y) ((get 'tag 'real)
		    (make-real (atan x y)))))
(put 'square 'scheme-number
     (lambda (x) (tag (square x))))
(put 'sqrt 'scheme-number
     (lambda (x) ((get 'tag 'real)
		  (make-real (sqrt x)))))

;; rationals
(put 'cos 'rational 
     (lambda (x) ((get 'tag 'real)
		  (make-real (cos (/ (numer x)
				     (denom x)))))))
(put 'sin 'rational 
     (lambda (x) ((get 'tag 'real)
		  (make-real (sin (/ (numer x)
				     (denom x)))))))
(put 'atan '(rational rational)
     (lambda (x y) ((get 'tag 'real)
		    (make-real (atan (/ (numer x) (denom x))
				     (/ (numer y) (denom y)))))))
(put 'square 'rational
     (lambda (x) (tag (make-rational (square numer)
				     (square denom)))))
(put 'sqrt 'rational
     (lambda (x) ((get 'tag 'real)
		  (make-real (sqrt x)))))

;; reals
(put 'cos 'real 
     (lambda (x) (tag (cos x))))
(put 'sin 'real 
     (lambda (x) (tag (sin x))))
(put 'atan '(real real)
     (lambda (x y) (tag (atan x y))))
(put 'square 'real
     (lambda (x) (tag (square x))))
(put 'sqrt 'real
     (lambda (x) (tag (sqrt x))))

;; Define generic procedures:
(define (cosine x) (apply-generic 'cos x))
(define (sine x) (apply-generic 'sin x))
(define (arctangent x) (apply-generic 'atan x))
(define (sqr x) (apply-generic 'square x))
(define (square-root x) (apply-generic 'sqrt x))


;; Now we can update the complex package with the generic procedures:
(define (install-rectangular-package)
  ;; internal procedures
  (define (real-part z) (car z))
  (define (imag-part z) (cdr z))
  (define (make-from-real-imag x y) (cons x y))
  (define (magnitude z)
    (square-root (add (sqr (real-part z))
		      (sqr (imag-part z)))))
  (define (angle z)
    (arctangent (imag-part z) (real-part z)))
  (define (make-from-mag-ang r a)
    (cons (mul r (cosine a)) (mul r (sine a))))
  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'rectangular x))
  (put 'real-part '(rectangular) real-part)
  (put 'imag-part '(rectangular) imag-part)
  (put 'magnitude '(rectangular) magnitude)
  (put 'angle '(rectangular) angle)
  (put 'make-from-real-imag 'rectangular
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'rectangular
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)


(define (install-polar-package)
  ;; internal procedures
  (define (magnitude z) (car z))
  (define (angle z) (cdr z))
  (define (make-from-mag-ang r a) (cons r a))
  (define (real-part z) (mul (magnitude z) (cosine (angle z))))
  (define (imag-part z) (mul (magnitude z) (sine (angle z))))
  (define (make-from-real-imag x y)
    (cons (square-root (add (sqr x) (sqr y)))
	  (arctangent y x)))
  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'polar x))
  (put 'real-part '(polar) real-part)
  (put 'imag-part '(polar) imag-part)
  (put 'magnitude '(polar) magnitude)
  (put 'angle '(polar) angle)
  (put 'make-from-real-imag 'polar
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'polar
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)


(define (install-complex-package)
  ;; imported procedures from rectangular and polar packages
  (define (make-from-real-imag x y)
    ((get 'make-from-real-imag 'rectangular) x y))
  (define (make-from-mag-ang r a)
    ((get 'make-from-mag-ang 'polar) r a))
  ;; internal procedures
  (define (add-complex z1 z2)
    (make-from-real-imag (add (real-part z1) (real-part z2))
			 (add (imag-part z1) (imag-part z2))))
  (define (sub-complex z1 z2)
    (make-from-real-imag (sub (real-part z1) (real-part z2))
			 (sub (imag-part z1) (imag-part z2))))
  (define (mul-complex z1 z2)
    (make-from-mag-ang (mul (magnitude z1) (magnitude z2))
		       (add (angle z1) (angle z2))))
  (define (div-complex z1 z2)
    (make-from-mag-ang (div (magnitude z1) (magnitude z2))
		       (sub (angle z1) (angle z2))))
  ;; interface to rest of the system
  (define (tag z) (attach-tag 'complex z))
  (put 'add '(complex complex)
       (lambda (z1 z2) (tag (add-complex z1 z2))))
  (put 'sub '(complex complex)
       (lambda (z1 z2) (tag (sub-complex z1 z2))))
  (put 'mul '(complex complex)
       (lambda (z1 z2) (tag (mul-complex z1 z2))))
  (put 'div '(complex complex)
       (lambda (z1 z2) (tag (div-complex z1 z2))))
  (put 'make-from-real-imag 'complex
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'complex
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)
