#lang sicp

;The square of each successive element in the list is being cons-ed on to the front of the list, so it goes in reverse order.

;In the second program, the data structure is inverted such that the car points to the next pair instead of the cdr. To fix, he needs to put the iter function inside the cons.