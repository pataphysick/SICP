#lang sicp

(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

(define (make-point x y)
  (cons x y))

(define (x-point p)
  (car p))

(define (y-point p)
  (cdr p))

(define (make-segment p q)
  (cons p q))

(define (start-segment s)
  (car s))

(define (end-segment s)
  (cdr s))

(define (make-rect1 p l h) ;define as anchor point (lower left corner, say) and length and width
  (cons p
        (cons l h)))

(define (make-rect2 s) ;define rectangle using diagonal described by line segment s
  (let ((x1 (x-point (start-segment s)))
        (y1 (y-point (start-segment s)))
        (x2 (x-point (end-segment s)))
        (y2 (y-point (end-segment s))))
    (define p (make-point (min x1 x2) (min y1 y2)))
    (define l (- (max x1 x2) (min x1 x2)))
    (define h (- (max y1 y2) (min y1 y2)))
    (cons p
          (cons l h))))

(define (length r)
  (car (cdr r)))

(define (height r)
  (cdr (cdr r)))

(define (perimeter r)
  (+ (* 2 (height r))
     (* 2 (length r))))

(define (area r)
  (* (height r)
     (length r)))