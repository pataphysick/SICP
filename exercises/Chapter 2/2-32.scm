#lang sicp

(define (subsets s)
  (if (null? s)
      (list nil)
      (let ((rest (subsets (cdr s))))
        (append rest
                (map (lambda (x) (cons (car s) x))
                     rest)))))

;The procedure works by starting at the rightmost part of the list (i.e. the empty list) and working back by adding the cons of the elements to the left with all the previous elements. Since all the elements are unique, cons-ing the new number into all the combinations of the old ones will produce further unique combinations until it yields all possible subsets.