#lang sicp

;a. Asssume that each division has a get-record procedure installed in the system, tagged by the division name.

(define (get-record name division)
  ((get 'get-record division) name))

;b. We are assuming each division has its own get-salary procedure since the records are already said to be structured differently in different divisions.

(define (get-salary name division)
  (let ((record (get-record name division)))
    ((get 'get-salary division) name)))

;c.

(define (find-employee-record name list)
  (if (null? list)
      '()
      (let ((record (get-record name (car list))))
        (if (null? record)
            (find-employee-record name (cdr list))
            record))))

;d. They must implement get-record, get-salary, etc. for the new division and install these procedures tagged with the company name into Insatiable's systems.