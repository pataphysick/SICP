#lang sicp

(define (make-interval a b) (cons a b))

(define (lower-bound i)
  (let ((a (car i))
        (b (cdr i)))
    (min a b)))

(define (upper-bound i)
  (let ((a (car i))
        (b (cdr i)))
    (max a b)))

(define (make-center-percent c p)
  (let ((w (* c (/ p 100))))
    (make-interval (- c w) (+ c w))))

(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))

(define (percent i)
  (let ((c (center i))
        (w (- (upper-bound i) (lower-bound i))))
    (* 100 (/ w c))))