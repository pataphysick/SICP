#lang sicp

(define (same-parity x . y)
  (define parity (even? x)) ;use parity = false for odd, parity = true for even
  (define (iter l1 l2)
    (cond ((null? l1)
           l2)
          ((equal? (even? (car l1))
                   parity)
           (cons (car l1)
                 (iter (cdr l1) l2)))
          (else (iter (cdr l1) l2))))
  (iter (cons x y) nil))