(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
	(apply proc (map contents args))
	(if (= (length args) 2)
	  (let ((type1 (car type-tags))
		(type2 (cadr type-tags))
		(a1 (car args))
		(a2 (cadr args)))
	    (let ((t1->t2 (get-coercion type1 type2))
		  (t2->t1 (get-coercion type2 type1)))
	      (cond (t1->t2
		      (apply-generic op (t1->t2 a1) a2))
		    (t2->t1
		      (apply-generic op a1 (t2->t1 a2)))
		    (else (error "No method for these types"
				 (list op type-tags))))))
	  (error "No method for these types"
		 (list op type-tags)))))))

(define (apply-generic op . args)
  (let ((arg1 (car args))
	(arg2 (cadr args)))
    (let ((type1 (type-tag arg1))
	  (type2 (type-tag arg2)))
      (if (equal? type1 type2)
	(apply op args)
	(let ((raised1 (successive-raise arg1 type2)))
	  (if raised1
	    (op raised1 arg2)
	    (let ((raised2 (successive-raise arg2 type1)))
	      (if raised2
		(op arg1 raised2)
		(error "No method for these types")))))))))

(define (successive-raise arg to-type)
  (let ((from-type (type-tag arg)))
    (let ((proc (get 'raise from-type)))
      (cond ((not proc) #f)
	    ((equal? from-type to-type) arg)
	    (else (successive-raise (apply proc arg) to-type))))))

